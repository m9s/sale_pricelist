#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Sale Pricelist',
    'name_de_DE': 'Verkauf Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Define pricelists for sales
''',
    'description_de_DE': '''
    - Ermöglicht die Verwendung von Preislisten im Modul Verkauf
''',
    'depends': [
        'sale',
        'account_invoice_pricelist'
    ],
    'xml': [
        'sale.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
