#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval, Get
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft')),
}
DEPENDS = ['state']


class Sale(ModelSQL, ModelView):
    "Sale"
    _description = __doc__
    _name = "sale.sale"

    pricelist = fields.Many2One('pricelist.pricelist', 'Pricelist',
            on_change=['pricelist'], states=STATES, required=True, domain=[
                ('active', '=', True),
                ('type', '=', 'sale'),
                ('company', '=', Eval('company')),
            ], depends=DEPENDS+['company'])

    def on_change_pricelist(self, vals):
        pricelist_obj = Pool().get('pricelist.pricelist')
        res = {}
        if vals.get('pricelist'):
            pricelist = pricelist_obj.browse(vals['pricelist'])
            if not pricelist.active==True:
                self.raise_user_error('pricelist_no_currency')
            else:
                res['currency'] = pricelist.currency.id
        return res

    def on_change_party(self, vals):
        party_obj = Pool().get('party.party')

        res = super(Sale, self).on_change_party(vals)
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            if party.pricelist_sale:
                if party.pricelist_sale.active==True:
                    res['pricelist'] = party.pricelist_sale.id
                else:
                    self.raise_user_warning('inactive_pricelist',
                            'cust_list_inactive')
        return res

    def create_invoice(self, sale_id):
        '''
        Create an invoice for the sale

        :param sale_id: the sale id

        :return: the created invoice id or None
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.journal')
        invoice_line_obj = pool.get('account.invoice.line')
        sale_line_obj = pool.get('sale.line')

        context = {}
        sale = self.browse(sale_id)

        invoice_lines = self._get_invoice_line_sale_line(sale)
        if not invoice_lines:
            return

        journal_id = journal_obj.search([
            ('type', '=', 'revenue'),
            ], limit=1)
        if journal_id:
            journal_id = journal_id[0]

        context['user'] = Transaction().user
        with Transaction().set_context(**context):
            with Transaction().set_user(0):
                invoice_id = invoice_obj.create({
                    'company': sale.company.id,
                    'type': 'out_invoice',
                    'reference': sale.reference,
                    'journal': journal_id,
                    'party': sale.party.id,
                    'invoice_address': sale.invoice_address.id,
                    'currency': sale.currency.id,
                    'account': sale.party.account_receivable.id,
                    'payment_term': sale.payment_term.id,
                    'pricelist': sale.pricelist.id,
                })

        for line_id in invoice_lines:
            for vals in invoice_lines[line_id]:
                vals['invoice'] = invoice_id
                with Transaction().set_context(**context):
                    with Transaction().set_user(0):
                        invoice_line_id = invoice_line_obj.create(vals)
                sale_line_obj.write(line_id, {
                    'invoice_lines': [('add', invoice_line_id)],
                    })

        with Transaction().set_context(**context):
            with Transaction().set_user(0):
                invoice_obj.update_taxes([invoice_id])

        self.write(sale_id, {
            'invoices': [('add', invoice_id)],
        })
        return invoice_id

Sale()


class SaleLine(ModelSQL, ModelView):
    _name = "sale.line"

    def __init__(self):
        super(SaleLine, self).__init__()
        fields = [self.product, self.quantity, self.unit]
        for field in fields:
            field = copy.copy(field)
            if field.on_change==None:
                field.on_change = []
            if '_parent_sale.pricelist' not in field.on_change:
                field.on_change += ['_parent_sale.pricelist']

        if not self.product.context:
            self.product.context = {
                    'pricelist': Get(Eval('_parent_sale', {}), 'pricelist'),
                    'currency': Get(Eval('_parent_sale', {}), 'currency')}
        else:
            if 'pricelist' not in self.product.context:
                   self.product.context['pricelist'] = \
                           Get(Eval('_parent_sale', {}), 'pricelist')
            if 'currency' not in self.product.context:
                   self.product.context['currency'] = \
                           Get(Eval('_parent_sale', {}), 'currency')

        self._reset_columns()

    def on_change_product(self, vals):
        product_obj = Pool().get('product.product')
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = super(SaleLine, self).on_change_product(vals)
        context = {}
        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])

        if vals.get('_parent_sale.currency'):
            context['currency'] = vals['_parent_sale.currency']
        if vals.get('unit'):
            context['uom'] = vals['unit']
        else:
            context['uom'] = product.sale_uom.id

        if vals.get('_parent_sale.pricelist'):
            context['pricelist'] = vals['_parent_sale.pricelist']

        with Transaction().set_context(**context):
            res['unit_price'] = pricelist_obj.get_price([product.id],
                quantity=vals.get('quantity', 1))[product.id]

        vals = vals.copy()
        vals['unit_price'] = res['unit_price']
        vals['type'] = 'line'
        res['amount'] = self.on_change_with_amount(vals)

        return res

    def on_change_quantity(self, vals):
        """
        on_change quantity is needed for price computation of block prices
        as they depend on the product quantity
        """
        product_obj = Pool().get('product.product')
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = super(SaleLine, self).on_change_quantity(vals)
        context = {}
        if not vals.get('product'):
            return res

        product = product_obj.browse(vals['product'])
        if vals.get('_parent_sale.currency'):
            context['currency'] = vals['_parent_sale.currency']
        if vals.get('unit'):
            context['uom'] = vals['unit']
        else:
            context['uom'] = product.sale_uom.id

        if vals.get('_parent_sale.pricelist'):
            context['pricelist'] = vals['_parent_sale.pricelist']

        with Transaction().set_context(**context):
            res['unit_price'] = pricelist_obj.get_price([product.id],
                    quantity=vals.get('quantity', 1), )[product.id]

        vals = vals.copy()
        vals['unit_price'] = res['unit_price']
        vals['type'] = 'line'
        res['amount'] = self.on_change_with_amount(vals)

        return res

SaleLine()
